using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIHandler : MonoBehaviour
{
    [SerializeField] private RectTransform _winText, _loseText;

    private void Start()
    {
        Observer.Instance.OnWinLevel += WinTextAnimation;
        Observer.Instance.OnLoseLevel += LoseTextAnimation;
    }

    public void WinTextAnimation()
    {
        StartCoroutine(TextAppearance(_winText));
    }
    public void LoseTextAnimation()
    {
        StartCoroutine(TextAppearance(_loseText));
    }

    private IEnumerator TextAppearance(RectTransform rect)
    {
        var startScale = rect.localScale;
        rect.localScale = Vector3.zero;
        rect.gameObject.SetActive(true);
        rect.DOScale(startScale, 0.5f);
        yield return new WaitForSeconds(1.5f);
        rect.DOScale(Vector3.zero, 0.5f);
        yield return new WaitForSeconds(0.5f);
        rect.gameObject.SetActive(false);
        rect.localScale = startScale;
    }

}
