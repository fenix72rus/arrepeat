using System;
using UnityEngine;

public class Observer : MonoBehaviour
{
    public static Observer Instance { get; private set;}

    public Action<InterectiveCube> OnCubeSelectedEvent = delegate { Debug.Log("OnCubeSelected"); };
    public Action OnWinLevel = delegate { Debug.Log("OnWinLevel"); };
    public Action OnLoseLevel = delegate { Debug.Log("OnLoseLevel"); };
    public bool IsReadyToPlay { get; set; }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
            return;
        }

        Destroy(this.gameObject);
    }

    public void WinLevel()
    {
        IsReadyToPlay = false;
        OnWinLevel();
    }

    public void LoseLevel()
    {
        IsReadyToPlay = false;
        OnLoseLevel();
    }
}
