using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchHandler : MonoBehaviour
{
    private Camera _camera;

    private void Awake()
    {
        _camera = Camera.main;   
    }

    private void Update()
    {
        var touches = Input.touches;
        if(touches.Length > 0)
        {
            foreach(var touch in touches)
            {
                if(touch.phase == TouchPhase.Began)
                {
                    TakeTouch(touch.position);
                }
            }
        }
        else
        {
            if(Input.GetMouseButtonDown(0))
            {
                TakeTouch(Input.mousePosition);
            }
        }
    }

    public void TakeTouch(Vector3 touchPosition)
    {
        if (!Observer.Instance.IsReadyToPlay)
            return;

        Ray ray = _camera.ScreenPointToRay(touchPosition);

        if(Physics.Raycast(ray, out var hit))
        {
            if(hit.transform.TryGetComponent(out InterectiveCube interective))
            {
                interective.Blink();
                Observer.Instance.OnCubeSelectedEvent(interective);
            }
        }
    }
}
