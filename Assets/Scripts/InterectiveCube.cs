using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Renderer))]
public class InterectiveCube : MonoBehaviour
{
    [SerializeField] private Color _colorful, _colorless;

    private Material _material;

    private void Awake()
    {
        _material = this.GetComponent<Renderer>().material;
        _material.color = _colorless;
    }

    public void Coloration()
    {
        _material.DOColor(_colorful, 0.4f);
    }

    public void Discoloration()
    {
        _material.DOColor(_colorless, 0.4f);
    }

    public void Blink()
    {
        Sequence sequence = DOTween.Sequence();

        _material.color = _colorless;
        sequence.Append(_material.DOColor(_colorful, 0.4f));
        sequence.Append(_material.DOColor(_colorless, 0.4f));
    }
}
