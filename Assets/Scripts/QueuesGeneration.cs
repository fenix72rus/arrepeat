using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueuesGeneration : MonoBehaviour
{
    [SerializeField] private InterectiveCube[] interectiveCubes;

    private Queue<InterectiveCube> currentCubesQueue, targetCubesQueue;

    private void Start()
    {
        Observer.Instance.OnCubeSelectedEvent += TakeCube;
    }

    public void GenerateQueue()
    {
        int queueLenght = Random.Range(3, 6);
        var newCubesQueue = new Queue<InterectiveCube>();

        for(int i = 0; i < queueLenght; i++)
        {
            var randomCube = interectiveCubes[Random.Range(0, interectiveCubes.Length)];
            newCubesQueue.Enqueue(randomCube);
        }

        targetCubesQueue = new Queue<InterectiveCube>(newCubesQueue);
        QueueVisualization();
    }

    public void QueueVisualization()
    {
        StartCoroutine(QueueAnimation());
    }

    private IEnumerator QueueAnimation()
    {
        var queue = targetCubesQueue;
        foreach(var cube in queue)
        {
            cube.Blink();
            yield return new WaitForSeconds(1f);
        }
        currentCubesQueue = new Queue<InterectiveCube>(targetCubesQueue);
        Observer.Instance.IsReadyToPlay = true;
    }

    public void TakeCube(InterectiveCube cube)
    {
        var targetCube = currentCubesQueue.Peek();

        if (targetCube == cube)
        {
            Debug.Log("Current cube");
            currentCubesQueue.Dequeue().Blink();
            if(currentCubesQueue.Count == 0)
            {
                Observer.Instance.WinLevel();
            }
        }
        else
        {
            Observer.Instance.LoseLevel();
        }
    }
}
